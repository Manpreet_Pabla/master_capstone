# README #

Master_Capstone is Food Ordering Solution App build on Mean Stack Framwork.

### What is this repository for? ###

Master_Capstone Version 1


### How do I get set up? ###

Frameworks  Required:

Express 4.17.1
React 16.13.1
Nodejs 12.18.4

Database:
Mongo DB

Languages Used:
C# , JavaScript, HTML , CSS , Bootstrap


### Contribution guidelines ###

Testing with JUNIT Test Cases.
Code review done for every commit by Team Lead.

License:
A software license tells others what they can and can't do with your source code, so it's important to make an informed decision.
Working on license From : Microsoft 365

### Who do I talk to? ###

Contact Person : Manpreet Pabla 
